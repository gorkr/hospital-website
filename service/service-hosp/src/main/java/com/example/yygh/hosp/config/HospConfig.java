package com.example.yygh.hosp.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author gorkr
 * @date 2022/01/29 12:13
 **/

@Configuration
@MapperScan("com.example.yygh.hosp.mapper")
public class HospConfig {
}
    