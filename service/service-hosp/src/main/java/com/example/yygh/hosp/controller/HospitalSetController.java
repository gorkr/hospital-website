package com.example.yygh.hosp.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.yygh.common.MD5;
import com.example.yygh.common.result.Result;
import com.example.yygh.hosp.service.HospitalSetService;
import com.example.yygh.model.hosp.HospitalSet;
import com.example.yygh.vo.hosp.HospitalSetQueryVo;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;

/**
 * @author gorkr
 * @date 2022/01/29 12:08
 **/

@RestController
@RequestMapping("/admin/hosp/hospitalSet")
public class HospitalSetController {

    @Autowired
    private HospitalSetService hospitalSetService;

    // 查询医院设置所有信息
    @ApiOperation(value = "获取所有医院设置")
    @GetMapping("findAll")
    public Result findAllHospitalSet(){
        // 调用service的方法
        final List<HospitalSet> list = hospitalSetService.list();
        return Result.ok(list);
    }

    // 逻辑删除医院设置
    // todo 记录不存在时的错误处理
    @ApiOperation(value = "逻辑删除医院设置")
    @DeleteMapping("id")
    public Result removeHospSet(@PathVariable Long id){
        final boolean b = hospitalSetService.removeById(id);
        if(b){
            return Result.ok();
        }else {
            return Result.fail();
        }
    }

    // 条件查询带分页
    @PostMapping("findPageHospSet/{current}/{limit}")  // @RequestBody 是json格式的提交
    public Result findPageHospSet(@PathVariable long current,
                                  @PathVariable long limit,
                                  @RequestBody(required = false)HospitalSetQueryVo hospitalSetQueryVo){
        Page<HospitalSet> page = new Page<>(current, limit);  // 第几页， 一页有多少记录
        final QueryWrapper<HospitalSet> wrapper = new QueryWrapper<>();
        final String hosname = hospitalSetQueryVo.getHosname();
        final String hoscode = hospitalSetQueryVo.getHoscode();
        if(!StringUtils.isEmpty(hosname)){
            wrapper.like("hosname", hospitalSetQueryVo.getHosname());
        }
        if(!StringUtils.isEmpty(hoscode)){
            wrapper.like("hoscode", hospitalSetQueryVo.getHoscode());
        }

        final Page<HospitalSet> pageHospitalSetPage = hospitalSetService.page(page, wrapper);
        return Result.ok(pageHospitalSetPage);
    }

    // 添加医院设置
    @PostMapping("saveHospitalSet")
    public Result saveHospitalSet(@RequestBody HospitalSet hospitalSet){
        hospitalSet.setStatus(1);
        final Random random = new Random();
        hospitalSet.setSignKey(MD5.encrypt(System.currentTimeMillis()+""+random.nextInt(1000)));

        final boolean save = hospitalSetService.save(hospitalSet);
        if(save){
            return Result.ok();
        }else {
            return Result.fail();
        }
    }

    // 根据id获取医院设置
    @GetMapping("getHospSet/{id}")
    public Result getHospSet(@PathVariable Long id){
        HospitalSet hospitalSet = hospitalSetService.getById(id);
        return Result.ok(hospitalSet);
    }

    // 修改医院设置
    @PostMapping("updateHospSet")
    public Result updateHospSet(@RequestBody HospitalSet hospitalSet){
        final boolean b = hospitalSetService.updateById(hospitalSet);
        if(b){
            return Result.ok();
        }else {
            return Result.fail();
        }
    }

    // 批量删除医院设置

    @DeleteMapping("batchRemove")
    public Result batchRemove(@RequestBody List<Long> idList){
        hospitalSetService.removeByIds(idList);
        return Result.ok();
    }

    @PutMapping("lockHosptialSet/{id}/{status}")
    public Result lockHsopSet(@PathVariable Long id,
                              @PathVariable Integer status){
        final HospitalSet hospitalSet = hospitalSetService.getById(id);
        hospitalSet.setStatus(status);
        hospitalSetService.updateById(hospitalSet);
        return Result.ok();
    }

}
    