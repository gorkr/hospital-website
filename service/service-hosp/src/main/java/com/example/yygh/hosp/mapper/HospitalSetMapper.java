package com.example.yygh.hosp.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.yygh.model.hosp.HospitalSet;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author gorkr
 * @date 2022/01/28 22:26
 **/
@Mapper
public interface HospitalSetMapper extends BaseMapper<HospitalSet> {
}
    