package com.example.yygh.hosp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.yygh.model.hosp.HospitalSet;

/**
 * @author gorkr
 * @title: HospitalSetService
 * @projectName yygh
 * @description: TODO
 * @date 2022/1/2822:31
 */

public interface HospitalSetService extends IService<HospitalSet> {
}
